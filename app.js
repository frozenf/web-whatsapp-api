// Load Library Requirement
const express = require('express');
const socketIO = require('socket.io');
const http = require('http');
const qrcode = require('qrcode');
const { Client, LocalAuth, MessageMedia } = require('whatsapp-web.js');

// Setup ENV Config
require('dotenv').config();

const app = express();
const server = http.createServer(app);
const io = socketIO(server);

app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.get('/',function(req,res){
  res.sendFile('index.html', {root: __dirname});
});

const client = new Client({
  restartOnAuthFail: true,
  authStrategy: new LocalAuth(),
  puppeteer: {
    headless: true,
    executablePath: '/usr/bin/chromium-browser',
    args: ['--no-sandbox']
  }
});

client.on('message', async msg => {
  if (msg.body === '/info') {
    let info = client.info;
    client.sendMessage(msg.from, `
            *Connection info*
            User name: ${info.pushname}
            My number: ${info.wid.user}
            Platform: ${info.platform}
        `);
  } else if (msg.body === '/groupinfo') {
    let chat = await msg.getChat();
    if (chat.isGroup) {
      msg.reply(`
                *Group Details*
                Name: ${chat.name}
                Description: ${chat.description}
                Created At: ${chat.createdAt.toString()}
                Created By: ${chat.owner.user}
                Participant count: ${chat.participants.length}
            `);
    } else {
      msg.reply('This command can only be used in a group!');
    }
  }
});

client.initialize();

// Setup Socket IO
var today  = new Date();
var now = today.toLocaleString();
io.on('connection', (socket) => {
  socket.emit('message', `${now} Connected`);

  client.on('qr', (qr) => {
    qrcode.toDataURL(qr, (err, url) => {
      socket.emit("qr", url);
      socket.emit('message', `${now} QR Code received`);
    });
  });

  client.on('ready', () => {
    socket.emit('message', `${now} WhatsApp is ready!`);
  });

  // When WhatsApp Authentication Successfully
  client.on('authenticated', () => {
    console.log('Authenticated');
    socket.emit('message', `${now} Whatsapp is authenticated!`);
  });

  // When WhatsApp Authentication Failed
  client.on('auth_failure', function(session) {
    socket.emit('message', `${now} Auth failure, restarting...`);
  });

  client.on('disconnected', function() {
    socket.emit('message', `${now} Disconnected`);
    client.destroy();
    client.initialize();
  });
});

app.post('/send_message', async (req, res, next) => {
  try {
    const { number, message } = req.body; // Get the body
    await client.sendMessage(`${number}@c.us`, message); // Send the message
    res.send({
      status : 200,
      message : "Pesan Whatsapp berhasil dikirimkan"
    }); // Send the response
  } catch (error) {
    res.status(500);
    res.send({
      status : 500,
      message : error.message
    });
  }
});

app.post('/send_file', async (req, res, next) => {
  try {
    const { number, base_64, caption } = req.body;
    const { fileTypeFromBuffer } = await import('file-type');
    const fileInfo = await fileTypeFromBuffer(Buffer.from(base_64, 'base64'))
    const media = await new MessageMedia(fileInfo.mime,base_64);
    await client.sendMessage(`${number}@c.us`, media, { caption: caption }); // Send the message
    res.send({
      status : 200,
      message : "Pesan Whatsapp berhasil dikirimkan"
    }); // Send the response
  } catch (error) {
    res.status(500);
    res.send({
      status : 500,
      message : error.message
    });
  }
});

app.post('/send_file_by_url', async (req, res, next) => {
  try {
    const { number, url, caption } = req.body; // Get the body
    const media = await MessageMedia.fromUrl(url);
    await client.sendMessage(`${number}@c.us`, media, { caption: caption }); // Send the message
    res.send({
      status : 200,
      message : "Pesan Whatsapp berhasil dikirimkan"
    }); // Send the response
  } catch (error) {
    res.status(500);
    res.send({
      status : 500,
      message : error.message
    });
  }
});

app.post('/send_file_by_path', async (req, res, next) => {
  try {
    const { number, path, caption } = req.body; // Get the body
    const media = await MessageMedia.fromFilePath(path);
    await client.sendMessage(`${number}@c.us`, media, { caption: caption }); // Send the message
    res.send({
      status : 200,
      message : "Pesan Whatsapp berhasil dikirimkan"
    }); // Send the response
  } catch (error) {
    res.status(500);
    res.send({
      status : 500,
      message : error.message
    });
  }
});

process.on("SIGINT", async () => {
  console.log("Shutdown System");
  await client.destroy();
  process.exit(0);
})

const PORT = process.env.PORT || 3000;
server.listen(PORT, () => {
  console.log('App listen on port ', PORT);
});


