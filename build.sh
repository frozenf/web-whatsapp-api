#!/bin/bash

docker rm --force wa_api
docker build . -t frozenf/exp-whatsapp
docker run -p 3000:3000 -d --name wa_api frozenf/exp-whatsapp
